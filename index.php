<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <link rel="stylesheet" href="css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  <link rel="stylesheet" href="css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
  <link rel="stylesheet" href="css/font-awesome.min.css">
  <link rel="stylesheet" href="template/style.css">
  <title>API Attendance</title>
</head>
<body>
  <?php
  include("./config/config.php");
  ?>

  <div class='container-fluid'>
  <?php
  include("core/header.php");
  ?>
  </div>

  <div class="container">
    <?php
    include("core/home.php")
    ?>
  </div>

  <script src="js/bootstrap.min.js"></script>
</body>
</html>
